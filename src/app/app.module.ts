import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { PhonesCrudComponent } from './phones-crud/phones-crud.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PhoneDetailComponent } from './phone-detail/phone-detail.component';
import { PhoneService } from './phone.service';

@NgModule({
  declarations: [
    AppComponent,
    PhonesCrudComponent,
    WelcomeComponent,
    PhoneDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [PhoneService],
  bootstrap: [AppComponent]
})
export class AppModule { }
