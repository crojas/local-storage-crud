import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhoneService } from '../phone.service';
import { Phone } from '../phone';

@Component({
  selector: 'app-phone-detail',
  templateUrl: './phone-detail.component.html',
  styleUrls: ['./phone-detail.component.css']
})
export class PhoneDetailComponent implements OnInit, OnDestroy {

  phone: Phone;
  private sub: any;
  constructor(private route: ActivatedRoute, private service: PhoneService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.phone = this.service.findByName(params['name']);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
