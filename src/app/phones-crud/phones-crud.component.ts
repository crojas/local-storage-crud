import { Component, OnInit } from '@angular/core';
import { Phone } from '../phone';
import { PhoneService } from '../phone.service';

@Component({
  selector: 'app-phones-crud',
  templateUrl: './phones-crud.component.html',
  styleUrls: ['./phones-crud.component.css']
})
export class PhonesCrudComponent implements OnInit {

  data: Phone[];
  current_phone: Phone;
  crud_operation = { is_new: false, is_visible: false };
  constructor(private service: PhoneService) {
  }

  ngOnInit() {
    this.data = this.service.read();
    this.current_phone = new Phone();
  }

  new() {
    this.current_phone = new Phone();
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }

  edit(row) {
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = false;
    this.current_phone = row;
  }

  delete(row) {
    this.crud_operation.is_new = false;
    const index = this.data.indexOf(row, 0);
    if (index > -1) {
      this.data.splice(index, 1);
    }
    this.save();
  }

  save() {
    if (this.crud_operation.is_new) {
      this.data.push(this.current_phone);
    }
    this.service.save(this.data);
    this.current_phone = new Phone();
    this.crud_operation.is_visible = false;
  }
}
