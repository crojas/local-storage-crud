import { Injectable } from '@angular/core';
import { Phone } from './phone';

@Injectable()
export class PhoneService {
  data: Phone[];
  constructor() {
    this.data = JSON.parse(localStorage.getItem('phones') || '[]');
  }

  read() {
    this.data = JSON.parse(localStorage.getItem('phones') || '[]');
    return this.data;
  }

  save(data: Phone[]) {
    this.data = data;
    localStorage.setItem('phones', JSON.stringify(this.data));
  }

  findByName(name: string) {
    return this.data.find(x => x.name === name);
  }
}
